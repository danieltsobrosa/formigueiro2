<?php namespace api\post;

use \core;
use \model\post;
use \util;

class ListTopicPostsHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		$topicId = $_PARAMS["topicId"];

		$listPosts = new post\ListTopicPostsModel();
		$listPosts->setTopicId($topicId);

  		$posts = $listPosts->execute();
		echo util\ResponseUtil::jsonOk( $posts );
	}
}
