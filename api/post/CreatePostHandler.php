<?php namespace api\post;

use \core;
use \model\post;
use \model\topic;
use \util;

class CreatePostHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		$body = util\RequestUtil::jsonBody( $this->request->getBody() );

	  	$topicId = $_PARAMS[ "topicId" ];

		$message = $body->message;
		$image = @$body->image;
		$geolocation = @$body->geolocation;

    	$createPost = new post\CreatePostModel();
		$createPost->setMessage( $message );
	    $createPost->setOwner( $loggedUser );
	    $createPost->setImage( $image );
	    $createPost->setGeolocation( $geolocation );

		$getTopic = new topic\GetTopicModel();
		$getTopic->setId( $topicId );
		$topic = $getTopic->execute();
		
	    $createPost->setTopic( $topic );
  		$post = $createPost->execute();

		echo util\ResponseUtil::jsonOk( $post );
	}
}
