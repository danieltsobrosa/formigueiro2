<?php namespace api\session;

use \core;
use \model\session;
use \util;

class LogoffHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
    $authToken = $_PARAMS[ "authToken" ];

    $createUser = new session\LogoffModel();
    $createUser->setAuthToken( $authToken );

    $createUser->execute();
    echo util\ResponseUtil::jsonOk( null );
	}
}
