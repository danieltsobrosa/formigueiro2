<?php namespace api\session;

use \core;
use \model\session;
use \util;

class LogonHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		$body = util\RequestUtil::jsonBody( $this->request->getBody() );

    $username = $body->username;
    $password = $body->password;

    $createUser = new session\LogonModel();
    $createUser->setUsername( $username );
    $createUser->setPassword( $password );

    $user = $createUser->execute();
    echo util\ResponseUtil::jsonOk( $user );
	}
}
