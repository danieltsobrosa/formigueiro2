<?php namespace api\session;

use \core;
use \model\session;
use \util;

class LoggedUserHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
	  	echo util\ResponseUtil::jsonOk( $loggedUser );
	}
}
