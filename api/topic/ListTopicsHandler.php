<?php namespace api\topic;

use \core;
use \model\topic;
use \util;

class ListTopicsHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
	    $listTopics = new topic\ListTopicsModel();

		if ( isset( $_GET[ "firstRow" ] ) ) {
			$listTopics->setFirstRow( $_GET[ "firstRow" ] );
		}

		if ( isset( $_GET[ "countLimit" ] ) ) {
			$listTopics->setCountLimit( $_GET[ "countLimit" ] );
		}

  		$topics = $listTopics->execute();
		echo util\ResponseUtil::jsonOk( $topics );
	}
}
