<?php namespace api\topic;

use \core;
use \model\topic;
use \model\post;
use \util;

class GetTopicHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
    $id = $_PARAMS[ "id" ];

    $getTopic = new topic\GetTopicModel();
		$getTopic->setId( $id );
  	$topic = $getTopic->execute();

		$listPosts = new post\ListTopicPostsModel();
		$listPosts->setTopicId( $id );
		$posts = $listPosts->execute();

		foreach ( $posts as &$post ) {
			$post->setTopic( null );
		}

		$topic->setPosts( $posts );

		echo util\ResponseUtil::jsonOk( $topic );
	}
}
