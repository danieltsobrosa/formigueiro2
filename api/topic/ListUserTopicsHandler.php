<?php namespace api\topic;

use \core;
use \model\topic;
use \util;

class ListUserTopicsHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
    $username = $_PARAMS[ "username" ];

    $listTopics = new topic\ListUserTopicsModel();
		$listTopics->setUsername( $username );

		if ( isset( $_GET[ "firstRow" ] ) ) {
			$listTopics->setFirstRow( $_GET[ "firstRow" ] );
		}

		if ( isset( $_GET[ "countLimit" ] ) ) {
			$listTopics->setCountLimit( $_GET[ "countLimit" ] );
		}

  	$topics = $listTopics->execute();
		echo util\ResponseUtil::jsonOk( $topics );
	}
}
