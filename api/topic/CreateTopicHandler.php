<?php namespace api\topic;

use \core;
use \model\topic;
use \util;

class CreateTopicHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		$body = util\RequestUtil::jsonBody( $this->request->getBody() );

    $title = $body->title;

    $createTopic = new topic\CreateTopicModel();
		$createTopic->setTitle( $title );
    $createTopic->setOwner( $loggedUser );

  	$topic = $createTopic->execute();
		echo util\ResponseUtil::jsonOk( $topic );
	}
}
