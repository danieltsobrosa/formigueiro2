<?php namespace api\test;

use \core;
use \model;
use \model\post;
use \model\topic;
use \util;

class TestHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		header("Content-Type: text/html");
		$body = util\RequestUtil::jsonBody( $this->request->getBody() );

		$text = $body->message;
		$found = model\util\PostUtil::findValidMentions($text);
		var_dump($found);

		echo util\ResponseUtil::jsonOk( null );
	}
}
