<?php namespace api\user;

use \core;
use \model\user;
use \util;

class GetLikeHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		$idOrigin = $loggedUser->getId();
		$idDestination = $_PARAMS["id"];
        
        $likeUser = new user\GetUserLikeModel();
    	$likeUser->setIdOrigin( $idOrigin );
    	$likeUser->setIdDestination( $idDestination );

      	$like = $likeUser->execute();
		echo util\ResponseUtil::jsonOk( $like );
	}
}
