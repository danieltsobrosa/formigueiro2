<?php namespace api\user;

use \core;
use \model\user;
use \util;

class CreateUserHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		$body = util\RequestUtil::jsonBody( $this->request->getBody() );

    $fullname = $body->fullname;
    $gender = $body->gender;
    $state = $body->state;
    $city = $body->city;
    $username = $body->username;
    $password = $body->password;
    $about = @$body->about;
    $avatar = @$body->avatar;

    $createUser = new user\CreateUserModel();
		$createUser->setFullname( $fullname );
    $createUser->setGender( $gender );
    $createUser->setState( $state );
    $createUser->setCity( $city );
    $createUser->setUsername( $username );
    $createUser->setPassword( $password );
    $createUser->setAbout( $about );
    $createUser->setAvatar( $avatar );

  	$user = $createUser->execute();
		echo util\ResponseUtil::jsonOk( $user );
	}
}
