<?php namespace api\user;

use \core;
use \model\user;
use \util;

class ListUsersHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		$filter = @$_GET["filter"];
        
        $listUsers = new user\ListUsersModel();
    	$listUsers->setFilter( $filter );

      	$users = $listUsers->execute();
		echo util\ResponseUtil::jsonOk( $users );
	}
}
