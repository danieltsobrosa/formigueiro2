<?php namespace api\user;

use \core;
use \model\user;
use \util;

class UnlikeUserHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		$idOrigin = $loggedUser->getId();
		$idDestination = $_PARAMS["id"];
        
        $likeUser = new user\UnlikeUserModel();
    	$likeUser->setIdOrigin( $idOrigin );
    	$likeUser->setIdDestination( $idDestination );

      	$likeUser->execute();
		echo util\ResponseUtil::jsonOk( null );
	}
}
