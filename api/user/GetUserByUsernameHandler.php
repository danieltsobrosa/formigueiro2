<?php namespace api\user;

use \core;
use \model\user;
use \util;

class GetUserByUsernameHandler extends core\APIHandler {

	public function execute( &$_PARAMS, &$loggedUser ) {
		$username = $_PARAMS[ "username" ];

		$getUser = new user\GetUserByUsernameModel();
		$getUser->setUsername($username);
	    $user = $getUser->execute();

	    echo util\ResponseUtil::jsonOk($user);
	}
}
