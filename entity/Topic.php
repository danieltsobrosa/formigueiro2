<?php namespace entity;

use \core;

/**
 * @Entity
 * @Table(name="topic")
 **/
class Topic implements \JsonSerializable
{
    use core\serializableTrait;

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(type="string", nullable=false)
     **/
    protected $title;

    /**
     * @Column(type="string", nullable=false)
     **/
    protected $category;

    /**
     * @Column(type="datetime", nullable=false)
     **/
    protected $creation;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="topics")
     **/
    protected $owner;

    /**
     * @OneToMany(targetEntity="Post", mappedBy="topic")
     **/
    protected $posts;

    /**
     * @Column(type="boolean", nullable=false)
     **/
    protected $closed = false;


    public function getId() {
        return $this->id;
    }

    public function setId( $id ) {
      $this->id = $id;
    }

    public function getTitle() {
      return $this->title;
    }

    public function setTitle( $title ) {
      $this->title = $title;
    }

    public function getCategory() {
      return $this->category;
    }

    public function setCategory( $category ) {
      $this->category = $category;
    }

    public function getCreation() {
      return $this->creation;
    }

    public function setCreation( $creation ) {
      $this->creation = $creation;
    }

    public function getOwner() {
      return $this->owner;
    }

    public function setOwner( $owner ) {
      $this->owner = $owner;
    }

    public function getPosts() {
      return $this->posts;
    }

    public function setPosts( $posts ) {
      $this->posts = $posts;
    }

    public function getClosed() {
      return $this->closed;
    }

    public function setClosed( $closed ) {
      $this->closed = $closed;
    }
}
