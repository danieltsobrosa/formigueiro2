<?php namespace entity;

use \core;

/**
 * @Entity
 * @Table(name="post")
 **/
class Post implements \JsonSerializable
{
    use core\serializableTrait;

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(type="string", nullable=false)
     **/
    protected $message;

    /**
     * @Column(type="datetime", nullable=false)
     **/
    protected $creation;

    /**
     * @Column(type="string", nullable=true)
     **/
    protected $image;

    /**
     * @Column(type="string", nullable=true)
     **/
    protected $geolocation;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="posts")
     **/
    protected $owner;

    /**
     * @ManyToOne(targetEntity="Topic", inversedBy="posts")
     **/
    protected $topic;


    public function getId() {
        return $this->id;
    }

    public function setId( $id ) {
      $this->id = $id;
    }

    public function getMessage() {
      return $this->message;
    }

    public function setMessage( $message ) {
      $this->message = $message;
    }

    public function getCreation() {
      return $this->creation;
    }

    public function setCreation( $creation ) {
      $this->creation = $creation;
    }

    public function getImage() {
      return $this->image;
    }

    public function setImage( $image ) {
      $this->image = $image;
    }

    public function getGeolocation() {
      return $this->geolocation;
    }

    public function setGeolocation( $geolocation ) {
      $this->geolocation = $geolocation;
    }

    public function getOwner() {
      return $this->owner;
    }

    public function setOwner( $owner ) {
      $this->owner = $owner;
    }

    public function getTopic() {
      return $this->topic;
    }

    public function setTopic( $topic ) {
      $this->topic = $topic;
    }
}
