<?php namespace entity;

use \core;
use \Doctrine\Common\Collections\ArrayCollection;

/**
 * @Entity
 * @Table(name="user")
 **/
class User implements \JsonSerializable
{
    use core\serializableTrait;

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(type="string", nullable=false)
     **/
    protected $fullname;

    /**
     * @Column(type="boolean", nullable=false)
     **/
    protected $gender; // true: Masculino, false: Feminino

    /**
     * @Column(type="string", nullable=false)
     **/
    protected $state;

    /**
     * @Column(type="string", nullable=false)
     **/
    protected $city;

    /**
     * @Column(type="string", nullable=false)
     **/
    protected $username;

    /**
     * @Column(type="string", nullable=false)
     **/
    protected $password;

    /**
     * @Column(type="string", nullable=true)
     **/
    protected $about;

    /**
     * @Column(type="string", nullable=true)
     **/
    protected $email;

    /**
     * @Column(type="string", nullable=false)
     **/
    protected $avatar = "default.png";

    /**
     * @Column(type="string", nullable=true)
     **/
    protected $authToken;

    /**
     * @OneToMany(targetEntity="Topic", mappedBy="onwer")
     **/
    protected $topics;

    /**
     * @OneToMany(targetEntity="Post", mappedBy="owner")
     **/
    protected $posts;


    public function __construct() {
      $this->topics = new ArrayCollection();
      $this->posts = new ArrayCollection();
    }


    public function getId() {
        return $this->id;
    }

    public function setId( $id ) {
      $this->id = $id;
    }

    public function getFullname() {
        return $this->fullname;
    }

    public function setFullname( $fullname ) {
        $this->fullname = $fullname;
    }

    public function getGender() {
        return $this->gender;
    }

    public function getGenderDescription() {
        if ($this->gender == 1) {
            return "Homem";
        }
        else{
            return "Mulher";
        }
    }

    public function setGender( $gender ) {
        $this->gender = $gender;
    }

    public function getState() {
        return $this->state;
    }

    public function setState( $state ) {
        $this->state = $state;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity( $city ) {
        $this->city = $city;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername( $username ) {
        $this->username = $username;
    }

    public function getPassword() {
      return $this->password;
    }

    public function setPassword( $password ) {
      $this->password = $password;
    }

    public function getAbout() {
        return $this->about;
    }

    public function setAbout( $about ) {
        $this->about = $about;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail( $email ) {
        $this->email = $email;
    }

    public function getAvatar() {
        return $this->avatar;
    }

    public function setAvatar( $avatar ) {
        if (is_null($avatar)) {
            $avatar = "default.png";
        }

        $this->avatar = $avatar;
    }

    public function getAuthToken() {
      return $this->authToken;
    }

    public function setAuthToken( $authToken ) {
      $this->authToken = $authToken;
    }

    public function clearTopicsAndPosts() {
        $this->topics = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }
}
