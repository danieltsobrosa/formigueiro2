<?php namespace entity;

use \core;

/**
 * @Entity
 * @Table(name="hashtag")
 **/
class Hashtag implements \JsonSerializable
{
    use core\serializableTrait;

    /**
     * @Id
     * @Column(type="integer")
     **/
    protected $idPost;

    /**
     * @Id
     * @Column(type="string")
     **/
    protected $name;


    public function getIdPost() {
      return $this->idPost;
    }

    public function setIdPost( $id ) {
      $this->idPost = $id;
    }

    public function getName() {
      return $this->name;
    }

    public function setName( $name ) {
      $this->name = $name;
    }
}
