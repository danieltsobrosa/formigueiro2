<?php namespace entity;

use \core;

/**
 * @Entity
 * @Table(name="mention")
 **/
class Mention implements \JsonSerializable
{
    use core\serializableTrait;

    /**
     * @Id
     * @Column(type="integer")
     **/
    protected $idMentionedUser;

    /**
     * @Id
     * @Column(type="integer")
     **/
    protected $idPost;


    public function getIdMentionedUser() {
      return $this->idMentionedUser;
    }

    public function setIdMentionedUser( $id ) {
      $this->idMentionedUser = $id;
    }

    public function getIdPost() {
      return $this->idPost;
    }

    public function setIdPost( $id ) {
      $this->idPost = $id;
    }
}
