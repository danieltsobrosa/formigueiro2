<?php namespace entity;

use \core;

/**
 * @Entity
 * @Table(name="user_like")
 **/
class Like implements \JsonSerializable
{
    use core\serializableTrait;

    /**
     * @Id
     * @Column(type="integer")
     **/
    protected $idUserOrigin;

    /**
     * @Id
     * @Column(type="integer")
     **/
    protected $idUserDestination;

    public function getIdUserOrigin() {
      return $this->idUserOrigin;
    }

    public function setIdUserOrigin( $id ) {
      $this->idUserOrigin = $id;
    }

    public function getIdUserDestination() {
      return $this->idUserDestination;
    }

    public function setIdUserDestination( $id ) {
      $this->idUserDestination = $id;
    }
}
