<?php
require_once 'vendor/autoload.php';

use \core;
use \util;

set_include_path( __DIR__ );

session_start();

$database = core\DatabaseManager::getInstance();
$database->init();

$api = core\APIManager::getInstance();
$api->init();

$view = core\ViewManager::getInstance();
$view->init();

$app = core\APIManager::getInstance()->getApp();

// Seta páginas de erro
$app->notFound( function() use ( $app ) {
  $app->render( "../errors/404.php" );
});


// Seta as definições dos parâmetros
$variables = util\FileUtil::readJSONObject( "config/variables.json" );
$map = array();

foreach ( $variables as $key => $value ) {
  $map[ $key ] = $value;
}

\Slim\Route::setDefaultConditions( $map );

$app->run();
