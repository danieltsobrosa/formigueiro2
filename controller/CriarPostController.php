<?php namespace controller;

use \core;
use \model\topic;
use \model\post;
use \controller;
use \util;

class CriarPostController extends core\Controller {

	public function execute( &$_PARAMS, &$template, &$loggedUser ) {

		if ( $template[ "METHOD" ] == "POST" ) {
			$topicId = $_PARAMS["topicId"];
			$message = $_POST["message"];
			$geolocation = @$_POST["geolocation"];
			$file = $_FILES["image"];
			$image = null;
			
			if ($file["size"] > 0) {
				$image = util\FileUtil::saveUploadedFile($file, "view/images/posts/", md5(microtime()));
			}

			// Obtém o tópico
			$topicModel = new topic\GetTopicModel();
			$topicModel->setId($topicId);
			$topic = $topicModel->execute();

			// Insere o primeiro post
			$postModel = new post\CreatePostModel();
			$postModel->setMessage($message);
			$postModel->setImage($image);
			$postModel->setGeolocation($geolocation);
			$postModel->setOwner($loggedUser);
			$postModel->setTopic($topic);
			$postModel->execute();

			// Redireciona para o perfil do usuário logado
			$template["app"]->redirect("/topico/".$topicId);

		} else {
			return "topico.php";
		}
	}
}
