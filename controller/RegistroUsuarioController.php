<?php namespace controller;

use \core;
use \model\user;
use \controller;

class RegistroUsuarioController extends core\Controller {

	public function execute( &$_PARAMS, &$template, &$loggedUser ) {

		$template["error"] = null;

		if ( $template[ "METHOD" ] == "POST" ) {
			$fullname = $_POST[ "fullname" ];
		    $gender = ($_POST[ "sex" ] === "true");
		    $state = $_POST[ "state" ];
		    $city = $_POST[ "city" ];
		    $username = $_POST[ "username" ];
		    $password = $_POST[ "password" ];
		    $email = $POST[ "email" ];
		    $about = @$_POST[ "about" ];
		    //$avatar = @$_POST[ "avatar" ];

		    $createUser = new user\CreateUserModel();
			$createUser->setFullname( $fullname );
		    $createUser->setGender( $gender );
		    $createUser->setState( $state );
		    $createUser->setCity( $city );
		    $createUser->setUsername( $username );
		    $createUser->setPassword( md5( $password ) );
		    $createUser->setAbout( $about );
		    $createUser->setEmail( $email );
		    //$createUser->setAvatar( $avatar );

		    try {
		    	$user = $createUser->execute();	
		    } catch (\Exception $e) {
		    	$template["error"] = $e->getMessage();
		    	if($template["error"] == "username-already-exists"){
              		$template["error"] = "Usuário já existe, escolha outro!";
              	} 
		    	return "registro.php";
		    }
		  	
			$template[ "METHOD" ] = "GET";
			$homeCtrl = new controller\HomeController();
			return $homeCtrl->execute( $_PARAMS, $template, $loggedUser );

		} else {
			return "registro.php";
		}
	}
}
