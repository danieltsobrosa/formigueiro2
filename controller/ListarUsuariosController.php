<?php namespace controller;

use \core;
use \model\user;
use \model\post;

class ListarUsuariosController {

	public function execute( &$_PARAMS, &$template, &$loggedUser ) {
		$template["users"] = array();
		$template["hashtags"] = array();

		if ( $template[ "METHOD" ] == "POST" ) {
	
			//critério de busca que vem da home
			$nomeProcurado = $_POST[ "nomeProcurado" ];

			$somenteHashtag = substr($nomeProcurado, 0, 1) == "#";
			$somenteUsuario = substr($nomeProcurado, 0, 1) == "@";

			if ($somenteUsuario || $somenteHashtag) {
				$nomeProcurado = substr($nomeProcurado, 1);
			}

			// Busca os usuários pelo nome
			if (!$somenteHashtag) {
				$userModel = new user\ListUsersModel();
				$userModel->setFilter( $nomeProcurado );
				$users = $userModel->execute();			
				$template[ "users" ] = $users;
			}

			// Busca as hashtags
			if (!$somenteUsuario) {
				$hashModel = new post\ListHashtagsModel();
				$hashModel->setFilter($nomeProcurado);
				$hashtags = $hashModel->execute();
				$template["hashtags"] = $hashtags;
			}
		}

		return "listar-usuarios.php";	
	}
}
