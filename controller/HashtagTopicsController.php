<?php namespace controller;

use \core;
	use \model\topic;

class HashtagTopicsController extends core\Controller {

	public function execute( &$_PARAMS, &$template, &$loggedUser ) {
		$hashtag = $_PARAMS["hashtag"];

		$topicModel = new topic\ListHashtagTopicsModel();
		$topicModel->setHashtag($hashtag);
		$topics = $topicModel->execute();

		$template["topics"] = $topics;

		return "topicos-por-hashtag.php";
	}
}
