<?php namespace controller;

use \core;
use \model\topic;
use \model\user;

class ProfileController extends core\Controller {
	public function execute( &$_PARAMS, &$template, &$loggedUser ) {
		$template["like"] = null;

		$username = $_PARAMS[ "username" ];

		$userModel = new user\GetUserByUsernameModel();
		$userModel->setUsername( $username );
		$user = $userModel->execute();

		$template[ "user" ] = $user;
		if (!is_null($loggedUser)) {
			$idOrigin = $loggedUser->getId();
			$idDestination = $user->getId();

			$getLikeModel = new user\GetUserLikeModel();
			$getLikeModel->setIdOrigin( $idOrigin );
			$getLikeModel->setIdDestination( $idDestination );
			$like = $getLikeModel->execute();
			if ( $template[ "METHOD" ] == "POST" ) {
				if ( !is_null( $like ) ) {
					$unlikeModel = new user\UnlikeUserModel();
					$unlikeModel->setIdOrigin( $idOrigin );
					$unlikeModel->setIdDestination( $idDestination );			
					$unlikeModel->execute();
				}
				else{
					$likeModel = new user\LikeUserModel();
					$likeModel->setIdOrigin( $idOrigin );
					$likeModel->setIdDestination( $idDestination );
					$likeModel->execute();
				}
			}

			$template[ "like" ] = $getLikeModel->execute();
		}

		$topicsModel = new topic\ListUserTopicsModel();
		$topicsModel->setUsername( $username );
		$topics = $topicsModel->execute();

		$template[ "topics" ] = $topics;

		return "profile.php";
	}
}
