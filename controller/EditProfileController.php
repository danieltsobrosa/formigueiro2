<?php namespace controller;

use \core;
use \model\topic;
use \model\user;
use \util;

class EditProfileController extends core\Controller {
	public function execute( &$_PARAMS, &$template, &$loggedUser ) {
		if ($template[ "METHOD" ] == "POST") {
			$fullname = $_POST[ "fullname" ];
		    $gender = ($_POST[ "sex" ] === "true");
		    $state = $_POST[ "state" ];
		    $city = $_POST[ "city" ];
		    $about = @$_POST[ "about" ];
		    $email = $_POST["email"];
		    $fileAvatar = $_FILES['avatar'];
		    $avatar = null;

		    if ($fileAvatar["size"] > 0) {
		    	$avatar = util\FileUtil::saveUploadedFile($fileAvatar, "view/images/profile/", $loggedUser->getId());
		    }else{
		    	$avatar = $loggedUser->getAvatar();
		    }

		    $updateUser = new user\UpdateUserModel();
		    $updateUser->setFullname( $fullname );
		    $updateUser->setGender( $gender );
		    $updateUser->setState( $state );
		    $updateUser->setCity( $city );
		    $updateUser->setEmail( $email );
		    $updateUser->setId($loggedUser->getId());
		    //$updateUser->setUsername( $loggedUser->getUserName() );
		    //$updateUser->setPassword( $loggedUser->getPassword()  );
		    $updateUser->setAbout( $about );
		    //$updateUser->setAuthToken($loggedUser->getAuthToken());
		    $updateUser->setAvatar( $avatar );

		    try {
		    	$loggedUser = $updateUser->execute();
	    		$template["app"]->redirect("/usuario/".$loggedUser->getUsername()."/perfil");

		    } catch (\Exception $e) {
		    	$template["error"] = $e->getMessage();
		    	echo $e->getMessage();
		    	return "home.php";
		    }

		}
		else{
		$template[ "masculinoChecked" ] = $loggedUser->getGender() ? "checked" : "";
		$template[ "femininoChecked" ] = $loggedUser->getGender() ? "" : "checked";
		$template[ "usuario" ] = $loggedUser;
		$template[ "isState" ] = function($state) use ($loggedUser) {
			return $loggedUser->getState() == $state? "selected": "";
		};

		return "editProfile.php";
		}
	}
}
