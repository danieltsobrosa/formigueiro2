<?php namespace controller;

use \core;
use \model\user;
use \model\post;

class PrincipalController extends core\Controller {

	public function execute( &$_PARAMS, &$template, &$loggedUser ) {
		$categoria = @$_GET["categoria"];

		$hashtagModel = new post\ListHashtagsModel();
		$stats = $hashtagModel->execute();
		$template["hashtags"] = array_slice($stats, 0, 5);

		$feedModel = new user\ListUserFeedsModel();
		$feedModel->setUser($loggedUser);
		$topics = $feedModel->execute();

		// Filtra pela categoria se veio o parâmetro
		if (is_null($categoria) || $categoria == "Todos") {
			$ret = $topics;
		} else {
			$ret = array();

			foreach ($topics as &$topic) {
				if ($topic->getCategory() == $categoria) {
					array_push($ret, $topic);
				}
			}
		}

		$template[ "topics" ] = $ret;

		return "principal.php";
	}
}
