<?php namespace controller;

use \core;
use \model\session;
use \controller;

class HomeController extends core\Controller {
	
	public function execute( &$_PARAMS, &$template, &$loggedUser ) {
		
		$isLogoff = (isset($_POST["logoff"]) && $_POST["logoff"] == "true" ? true : false);

		if(!$isLogoff && !is_null($loggedUser)){
			$this->redirect( "/principal" );
		}

		if ( $template[ "METHOD" ] == "POST" ) {
			if ($isLogoff) {
					$_SESSION[ "authToken" ] = null;
					$this->redirect( "/home" );

			} else {
				$username = $_POST[ "username" ];
			    $password = $_POST[ "password" ];

			    $createUser = new session\LogonModel();
			    $createUser->setUsername( $username );
			    $createUser->setPassword( md5( $password ) );

			    try{
			    	$user = $createUser->execute();
				}catch(\Exception $e){
					$template["errorMessage"] = $e->getMessage();
					return "home.php";
				}
				
				$authToken = $user->getAuthToken();
				$_SESSION[ "authToken" ] = $authToken;

				$this->redirect( "/principal" );
			}

		} else {
			return "home.php";
		}
	}
}
