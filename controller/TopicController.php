<?php namespace controller;

use \core;
use \model\topic;
use \model\user;
use \model\util;
use \Doctrine\Common\Collections\ArrayCollection;

class TopicController extends core\Controller {
	public function execute( &$_PARAMS, &$template, &$loggedUser ) {
		$id = $_PARAMS[ "id" ]; 
		$topicModel = new topic\GetTopicModel();
		$topicModel->setId( $id );
		$topic = $topicModel->execute();

		$posts = $topic->getPosts();

		$iterator = $posts->getIterator();
		$iterator->uasort(function ($a, $b) {
		    return ($a->getId() < $b->getId()) ? -1 : 1;
		});
		$topic->setPosts( new ArrayCollection(iterator_to_array($iterator)) );

		$template[ "topic" ] = $topic;
		
		$template["prepareMessage"] = function($message) {
			$ret = util\PostUtil::validMentionsToLinks($message);
			$ret = util\PostUtil::validHashtagsToLinks($ret);
			$ret = util\PostUtil::adjustNewLines($ret);
			return $ret;
		};

		return "topico.php";
	}
}
