<?php namespace controller;

use \core;
use \model\topic;
use \util;

class ConcluirTopicoController extends core\Controller {

	public function execute( &$_PARAMS, &$template, &$loggedUser ) {

		if ( $template[ "METHOD" ] == "POST" ) {
			$id = $_PARAMS["id"];

			$finishTopic = new topic\FinishTopicModel();
			$finishTopic->setId($id);
			$finishTopic->execute();

			$template["app"]->redirect("/principal");
		}
	}
}
