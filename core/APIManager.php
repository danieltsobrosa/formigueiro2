<?php namespace core;

use \entity;
use \core;
use \util;

class APIManager {
	private static $instance;
	private function __contruct() {}

	public static function getInstance() {
		if ( !isset( static::$instance ) ) {
			static::$instance = new APIManager();
		}

		return static::$instance;
	}

	//------------------------------------------------------------------

	private $app;

  private function getParams( $url, $arr ) {
    $params = array();
    $keys = array();
    $tokens = explode( "/", $url );

    foreach ( $tokens as $token ) {
			$t = str_replace( "(", "", $token );
			$t = str_replace( ")", "", $t );

      if ( util\StringUtil::startsWith( $t, ":" ) ) {
        array_push( $keys, $t );
      }
    }

    for ( $i = 0; $i < sizeof( $arr ); $i++ ) {
      $key = substr( $keys[ $i ], 1 );
      $params[ $key ] = $arr[ $i ];
    }

    return $params;
  }

	public function init() {
		$app = new \Slim\Slim( array(
			"templates.path" => __DIR__ . "/../view/templates"
		));

		$path = "config/api.json";
		$routes = util\FileUtil::readJSONObject( $path )->routes;

		foreach ( $routes as $route ) {
			$cls = "\\api\\" . str_replace("/", "\\", $route->handler );
			$method = mb_strtolower( $route->method );
			$url = "/api" . $route->url . "(/)";
			$public = isset( $route->public ) ? $route->public : false;
			//echo $method . " " . $url . "  -  " . $cls . "<br>";

			$app->$method( $url, function() use ( $app, $cls, $url, $public ) {
				$api = new $cls();
				$api->setRequest( $app->request );
				$api->setResponse( $app->response );
        		$_PARAMS = $this->getParams( $url, func_get_args() );
				$contentType = "application/json";

				// Obtém o token de autenticação
				$authToken = @$_PARAMS[ "authToken" ];

				if ( is_null( $authToken ) ) {
					try {
						$body = util\RequestUtil::jsonBody( $app->request->getBody() );
						$authToken = @$body->authToken;
					} catch ( \Exception $e ) {}
				}

				if ( is_null( $authToken ) ) {
					$authToken = @$_POST[ "authToken" ];
				}

				if ( is_null( $authToken ) ) {
					$authToken = @$_GET[ "authToken" ];
				}

				// Obtém o usuário logado
				$loggedUser = null;

				if ( !is_null( $authToken ) ) {
					$loggedUser = util\SessionUtil::getLoggedUser( $authToken );
				}

				// Se não for público e não estiver logado
				if ( !$public && is_null( $loggedUser ) ) {
					$e = new \Exception( "user-not-logged" );
					echo util\ResponseUtil::jsonError( $e );
					exit;
				}

				try {
        	$ct = $api->execute( $_PARAMS, $loggedUser );

					if ( !empty( $ct ) ) {
						$contentType = $ct;
					}

				} catch ( \Exception $e ) {
					echo util\ResponseUtil::jsonError( $e );
				}

				header( "Content-Type: " . $contentType );
        exit;
      });
		}

		$this->app = $app;
	}

	public function getApp() {
		return $this->app;
	}
}
