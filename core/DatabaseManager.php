<?php namespace core;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use \util;

class DatabaseManager {
	private static $instance;
	private function __contruct() {}

	public static function getInstance() {
		if ( !isset( static::$instance ) ) {
			static::$instance = new DatabaseManager();
		}

		return static::$instance;
	}

	//------------------------------------------------------------------

	private $entityManager;

	public function init() {
		$paths = array("entity");
		$isDevMode = false;

		$path = "config/database.json";
		$cfgFile = util\FileUtil::readJSONObject( $path );

		// the connection configuration
		$dbParams = array(
			"driver"   => $cfgFile->driver,
			"user"     => $cfgFile->user,
			"password" => $cfgFile->password,
			"dbname"   => $cfgFile->dbname
		);

		$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
		$config->setProxyDir( __DIR__ . '/../entity/proxies');
		$this->entityManager = EntityManager::create($dbParams, $config);
	}

	public function resetEntityManager() {
		if (!is_null($this->entityManager)) {
			$this->entityManager->close();
			$this->entityManager = null;
		}

		$this->init();
		return $this->getEntityManager();
	}

	public function getEntityManager() {
		return $this->entityManager;
	}
}
