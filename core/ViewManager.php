<?php namespace core;

use \entity;
use \core;
use \util;
use \model\session;

class ViewManager {
  private static $instance;

  private function __contruct() {}

  public static function getInstance() {
    if ( !isset( static::$instance ) ) {
      static::$instance = new ViewManager();
    }

    return static::$instance;
  }

  //------------------------------------------------------------------

  private $mimeTypes = array(
    "gif" => "image/gif",
    "png" => "image/png",
    "jpg" => "image/jpg",
    "css" => "text/css",
    "js" => "text/javascript",
    "html" => "text/html"
  );

  public function getMimeType( $filename ) {
    $ext = substr( strrchr( $filename, "." ), 1 );

    if ( array_key_exists( $ext, $this->mimeTypes ) ) {
      return $this->mimeTypes[ $ext ];
    }

    return "text/plain";
  }

  private function getParams( $url, $arr ) {
    $params = array();
    $keys = array();
    $tokens = explode( "/", $url );

    foreach ( $tokens as $token ) {
			$t = str_replace( "(", "", $token );
			$t = str_replace( ")", "", $t );

      if ( util\StringUtil::startsWith( $t, ":" ) ) {
        array_push( $keys, $t );
      }
    }

    for ( $i = 0; $i < sizeof( $arr ); $i++ ) {
      $key = substr( $keys[ $i ], 1 );
      $params[ $key ] = $arr[ $i ];
    }

    return $params;
  }

  public function init() {
    $app = core\APIManager::getInstance()->getApp();

    $path = "config/view.json";
    $config = util\FileUtil::readJSONObject( $path );

    // Mapeia as páginas
    $routes = $config->routes;

    foreach ( $routes as $route ) {
      $cls = "\\controller\\" . str_replace("/", "\\", $route->controller );
      $method = $route->method;
      $url = $route->url;
      $public = isset( $route->public ) ? $route->public : false;

      $app->$method( $url, function() use ( $app, $cls, $url, $public, $config ) {
        $ctrl = new $cls();
        $_PARAMS = $this->getParams( $url, func_get_args() );
        $loggedUser = null;

        //if ( !$public ) {
          $authToken = @$_SESSION[ "authToken" ];
               
          // Se a página é privada e não tem token     
          if ( !$public && is_null( $authToken ) ) {
            $app->render( $config->authPage );
            exit;

          // Se tem token
          } else if (!is_null($authToken)) {
            $sessionModel = new session\LoggedUserModel();
            $sessionModel->setAuthToken( $authToken );
            $loggedUser = $sessionModel->execute();

            // Se não tiver usuário logado com este token
            if ( is_null( $loggedUser ) ) {
              $_SESSION[ "authToken" ] = null;

              // Redireciona pra tela de logon
              $app->redirect( "/home" );
              exit;
            }
          }
        //}

        $templateData = array(
          "app" => $app,
          "request" => $app->request,
          "response" => $app->response,
          "_PARAMS" => $_PARAMS,
          "METHOD" => $app->request->getMethod(),
          "loggedUser" => $loggedUser
        );

        $templateName = $ctrl->execute( $_PARAMS, $templateData, $loggedUser );

        $app->render( $templateName, $templateData );
      });
    }

    // Mapeia os diretórios
    $directories = $config->static;

    foreach ( $directories as $dir ) {
      $url = $dir . "/:filename";

      $app->get( $url, function( $filename ) use ( $app, $dir ) {
        $file = __DIR__ . "/../view" . $dir . "/" . $filename;

        if ( file_exists( $file ) ) {
          header( "Content-Type: " . $this->getMimeType( $file ) );
          echo file_get_contents( $file );
        } else {
          $app->render( "../errors/404.php" );
        }
        exit;
      });
    }
  }
}
