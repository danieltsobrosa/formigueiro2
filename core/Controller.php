<?php namespace core;

abstract class Controller {
  private $app;

  protected function redirect( $url ) {
    header( "Location: " . $url );
    exit;
  }

  public abstract function execute( &$_PARAMS, &$template, &$loggedUser );

}
