<?php
namespace core;

abstract class APIHandler {
	protected $request;
	protected $response;

	public function getRequest() {
		return $this->request;
	}

	public function setRequest( $request ) {
		$this->request = $request;
	}

	public function getResponse() {
		return $this->response;
	}

	public function setResponse( $response ) {
		$this->response = $response;
	}

	public abstract function execute( &$_PARAMS, &$loggedUser );
}
