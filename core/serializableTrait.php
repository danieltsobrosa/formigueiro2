<?php namespace core;

trait serializableTrait {
    public function jsonSerialize() {
        return get_object_vars( $this );
    }
}
