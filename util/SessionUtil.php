<?php namespace util;

use \model\session;

class SessionUtil {

  public static function getLoggedUser( $authToken ) {
    $sessionModel = new session\LoggedUserModel();
    $sessionModel->setAuthToken( $authToken );

    return $sessionModel->execute();
  }
}
