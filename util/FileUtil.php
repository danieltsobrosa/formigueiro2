<?php
namespace util;

class FileUtil {

  public static function readJSONObject( $fileName ) {
    $root = __DIR__ . "/../";
    return json_decode( file_get_contents( $root . $fileName ) );
  }

  public static function readJSONArray( $fileName ) {
    return json_decode( file_get_contents( $_SERVER[ "DOCUMENT_ROOT" ] . "/" . $fileName ), true );
  }

  public static function getExt($filename) {
  	return substr($filename, strpos($filename,'.'), strlen($filename)-1);
  }

  public static function saveUploadedFile($srcFile, $destDir, $destFilename) {
  	$filename = $srcFile["name"];
    $ext = static::getExt($filename);
    $base = $_SERVER["DOCUMENT_ROOT"] . $destDir;
    $dest = $base . $destFilename . $ext;

    if (!file_exists($base)) {
      mkdir($base, 0777, true);
    }
    copy($srcFile["tmp_name"], $dest);
    return $destFilename . $ext;
  }
}