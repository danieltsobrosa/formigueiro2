<?php namespace util;

class RequestUtil {

  public static function jsonBody( $body ) {
    return json_decode( $body );
  }
}
