<?php namespace util;

class ResponseUtil {

  public static function jsonOk( $data ) {
    return json_encode(array(
      "ok" => true,
      "data" => $data
    ));
  }

  public static function jsonError( $error ) {
    return json_encode(array(
      "ok" => false,
      "message" => $error->getMessage()
    ));
  }
}
