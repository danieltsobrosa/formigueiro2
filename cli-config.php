<?php
use Doctrine\ORM\Tools\Console\ConsoleRunner;

use \core;

$dbManager = core\DatabaseManager::getInstance();
$dbManager->init();
$entityManager = $dbManager->getEntityManager();

return ConsoleRunner::createHelperSet($entityManager);