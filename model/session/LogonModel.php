<?php namespace model\session;

use \core;
use \entity;

class LogonModel {
  protected $username;
  protected $password;

  public function setUsername( $username ) {
    $this->username = $username;
  }

  public function setPassword( $password ) {
    $this->password = $password;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $userRepository = $entityManager->getRepository( '\entity\User' );

    // Busca o usuário pelo username
    $user = $userRepository->findOneBy( array(
      "username" => $this->username
    ));

    // Se não achou
    if ( is_null( $user ) ) {
      throw new \Exception( "invalid-user-or-password" );
    }

    // Se o usuário já possui um token de autenticação
    //if ( !empty( $user->getAuthToken() ) ) {
    //  throw new \Exception( "user-is-already-logged" );
    //}

    // Se a senha não confere
    if ( $this->password !== $user->getPassword() ) {
      throw new \Exception( "invalid-user-or-password" );
    }

    $now = date(' l jS \of F Y h:i:s A' );
    $seed = $now . "a" . rand( 0, 100 ) . "b" . rand( 0, 100 ) . "c" . rand( 0, 100 );
    $authToken = md5( $seed );

    $user->setAuthToken( $authToken );
    $entityManager->flush();

    $user->setPassword( null );
    return $user;
  }
}
