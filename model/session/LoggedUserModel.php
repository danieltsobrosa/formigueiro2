<?php namespace model\session;

use \core;
use \entity;

class LoggedUserModel {
  private $authToken;

  public function setAuthToken( $authToken ) {
    $this->authToken = $authToken;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $userRepository = $entityManager->getRepository( '\entity\User' );
    $user = $userRepository->findOneBy( array(
      "authToken" => $this->authToken
    ));

    return $user;
  }
}
