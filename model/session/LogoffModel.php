<?php namespace model\session;

use \core;
use \entity;

class LogoffModel {
  protected $authToken;

  public function setAuthToken( $authToken ) {
    $this->authToken = $authToken;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $userRepository = $entityManager->getRepository( '\entity\User' );

    // Busca o usuário pelo username
    $user = $userRepository->findOneBy( array(
      "authToken" => $this->authToken
    ));

    // Se não achou
    if ( is_null( $user ) ) {
      throw new \Exception( "user-does-not-exists" );
    }

    $user->setAuthToken( null );
    $entityManager->flush();
  }
}
