<?php namespace model\user;

use \core;
use \entity;

class GetUserLikeModel {
  protected $idOrigin;
  protected $idDestination;

  public function setIdOrigin( $id ) {
      $this->idOrigin = $id;
  }

  public function setIdDestination( $id ) {
      $this->idDestination = $id;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $userRepository = $entityManager->getRepository( '\entity\Like' );
    $like = $userRepository->findOneBy( array(
      "idUserOrigin" => $this->idOrigin,
      "idUserDestination" => $this->idDestination
    ));

    return $like;
  }
}
