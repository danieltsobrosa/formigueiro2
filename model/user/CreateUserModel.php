<?php namespace model\user;

use \core;
use \entity;

class CreateUserModel {
  protected $fullname;
  protected $gender; // true: Masculino, false: Feminino
  protected $state;
  protected $city;
  protected $username;
  protected $password;
  protected $about;
  protected $avatar;
  protected $email;

  public function setFullname( $fullname ) {
      $this->fullname = $fullname;
  }

  public function setGender( $gender ) {
      $this->gender = $gender;
  }

  public function setState( $state ) {
      $this->state = $state;
  }

  public function setCity( $city ) {
      $this->city = $city;
  }

  public function setUsername( $username ) {
      $this->username = $username;
  }

  public function setPassword( $password ) {
    $this->password = $password;
  }

  public function setAbout( $about ) {
      $this->about = $about;
  }

  public function setAvatar( $avatar ) {
      $this->avatar = $avatar;
  }

  public function setEmail( $email ){
      $this->email = $email;
  }

  public function execute() {
    $user = new entity\User();
    $user->setFullname( $this->fullname );
    $user->setGender( $this->gender );
    $user->setState( $this->state );
    $user->setCity( $this->city );
    $user->setUsername( $this->username );
    $user->setPassword( $this->password );
    $user->setAbout( $this->about );
    $user->setAvatar( $this->avatar );
    $user->setEmail( $this->email );

    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $userRepository = $entityManager->getRepository( '\entity\User' );
    $found = $userRepository->findOneBy( array(
      "username" => $user->getUsername()
    ));

    if ( !is_null( $found ) ) {
      throw new \Exception( "username-already-exists" );
    }

    $entityManager->persist( $user );
    $entityManager->flush();

    $user->setPassword( null );
    return $user;
  }
}
