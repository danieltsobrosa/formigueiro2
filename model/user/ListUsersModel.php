<?php namespace model\user;

use \core;
use \entity;
use \Doctrine\ORM\Query\ResultSetMapping;

class ListUsersModel {
  protected $filter;

  public function setFilter( $filter ) {
      $this->filter = $filter;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $qb = $entityManager->createQueryBuilder();

    if (is_null($this->filter)) {
      $filter = "%";
    } else {
      $filter = "%" . $this->filter . "%";
    }

    $query = $qb->select('u')
       ->from('entity\User', 'u')
       ->where('lower(u.fullname) like lower(?1)')
       ->orderBy('u.fullname', 'ASC')
       ->setParameter(1, $filter)
       ->getQuery();
    $users = $query->getResult();
    //var_dump($query);
    //var_dump($filter);

    if ( is_null( $users ) ) {
      return array();
    }

    return $users;
  }
}
