<?php namespace model\user;

use \core;
use \entity;

class UpdateUserModel {
  protected $id;
  protected $fullname;
  protected $gender; // true: Masculino, false: Feminino
  protected $state;
  protected $city;
  //protected $username;
  //protected $password;
  protected $email;
  protected $about;
  protected $avatar;

  public function setId( $id ) {
    $this->id = $id;
  }

  public function setFullname( $fullname ) {
      $this->fullname = $fullname;
  }

  public function setGender( $gender ) {
      $this->gender = $gender;
  }

  public function setState( $state ) {
      $this->state = $state;
  }

  public function setCity( $city ) {
      $this->city = $city;
  }

  //public function setUsername( $username ) {
  //    $this->username = $username;
  //}

  //public function setPassword( $password ) {
  //  $this->password = $password;
  //}

  public function setAbout( $about ) {
      $this->about = $about;
  }

  public function setAvatar( $avatar ) {
      $this->avatar = $avatar;
  }

  public function setEmail( $email ){
     $this->email = $email;
  }

  public function execute() {
    $user = new entity\User();

    $user->setId( $this->id );
    $user->setFullname( $this->fullname );
    $user->setGender( $this->gender );
    $user->setState( $this->state );
    $user->setCity( $this->city );
    $user->setAbout( $this->about );
    $user->setAvatar( $this->avatar );
    $user->setEmail( $this->email );

    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $userRepository = $entityManager->getRepository( '\entity\User' );
    $found = $userRepository->find( $user->getId() );

    if ( is_null( $found ) ) {
      throw new \Exception( "user-does-not-exists" );
    }

    $user->setUsername( $found->getUsername() );
    $user->setPassword( $found->getPassword() );
    $user->setAuthToken( $found->getAuthToken() );

    $entityManager->merge( $user );
    $entityManager->flush();

    $user->setPassword( null );
    return $user;
  }
}
