<?php namespace model\user;

use \core;
use \entity;

class UnlikeUserModel {
  protected $idOrigin;
  protected $idDestination;

  public function setIdOrigin( $id ) {
      $this->idOrigin = $id;
  }

  public function setIdDestination( $id ) {
      $this->idDestination = $id;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->resetEntityManager();
    $userRepository = $entityManager->getRepository( '\entity\Like' );
    $like = $userRepository->findOneBy( array(
      "idUserOrigin" => $this->idOrigin,
      "idUserDestination" => $this->idDestination
    ));

    if ( is_null( $like ) ) {
      throw new \Exception( "like-does-not-exists" );
    }

    $entityManager->remove( $like );
    $entityManager->flush();

    return $like;
  }
}
