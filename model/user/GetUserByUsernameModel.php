<?php namespace model\user;

use \core;
use \entity;

class GetUserByUsernameModel {
  protected $username;

  public function setUsername( $username ) {
      $this->username = $username;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $userRepository = $entityManager->getRepository( '\entity\User' );
    $user = $userRepository->findOneBy( array(
      "username" => $this->username
    ));

    if ( is_null( $user ) ) {
      throw new \Exception( "user-not-found" );
    }

    $user->setPassword(null);
    $user->clearTopicsAndPosts();
    
    return $user;
  }
}
