<?php namespace model\user;

use \core;
use \entity;
use \model;

class ListUserFeedsModel {
  protected $user;

  public function setUser($user) {
    $this->user = $user;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();

    $ret = array();

    $this->findByMentions($entityManager, $ret);
    $this->findByLikes($entityManager, $ret);

    usort($ret, function($a, $b) {
      return $a->getId() > $b->getId() ? -1 : 1;
    });

    return $ret;
  }

  private function execSQL($entityManager, $sql) {
    $query = $entityManager->createQuery($sql);
    $query->setFirstResult(0);
    $query->setMaxResults(50);
    return $query->getResult();
  }

  private function findByMentions($entityManager, &$ret) {
    // Procura pela mentions ao usuário
    $sql = "SELECT m FROM entity\Mention m WHERE m.idMentionedUser = " . $this->user->getId();
    $mentions = $this->execSQL($entityManager, $sql);

    // Monta a lista de id dos posts para ser usado no IN do SQL
    $postIds = array();

    foreach ($mentions as &$mention) {
      $postId = $mention->getIdPost();

      if (!in_array($postId, $postIds)) {
        array_push($postIds, $postId);
      }
    }

    if (count($postIds) > 0) {
      // Busca os posts onde o usuário foi mencionado
      $sql = "SELECT p FROM entity\Post p WHERE p.id in( " . implode(", ", $postIds) . " )";

      $posts = $this->execSQL($entityManager, $sql);

      // Monta a lista com os tópicos dos posts
      foreach ($posts as &$post) {
        $topic = $post->getTopic();

        if (!in_array($topic, $ret) && !$topic->getClosed()) {
          array_push($ret, $topic);
        }
      }
    }

    return $ret;
  }

  private function findByLikes($entityManager, &$ret) {
    // Procura pelos likes do usuário
    $sql = "SELECT l FROM entity\Like l WHERE l.idUserOrigin = " . $this->user->getId();
    $likes = $this->execSQL($entityManager, $sql);

    // Monta a lista de id dos usuários para ser usado no IN do SQL
    $userIds = array();

    foreach ($likes as &$like) {
      $userId = $like->getIdUserDestination();

      if (!in_array($userId, $userIds)) {
        array_push($userIds, $userId);
      }
    }

    if (count($userIds) > 0) {
      // Buscar todos os posts dos usuários destino destes likes
      $sql = "SELECT p FROM entity\Post p WHERE p.owner in( " . implode(",", $userIds) . ")";
      $posts = $this->execSQL($entityManager, $sql);

      // Monta a lista com os tópicos dos posts
      foreach ($posts as &$post) {
        $topic = $post->getTopic();

        if (!in_array($topic, $ret) && !$topic->getClosed()) {
          array_push($ret, $topic);
        }
      }
    }

    return $ret;
  }
}
