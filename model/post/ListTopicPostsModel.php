<?php namespace model\post;

use \core;
use \entity;
use \model;

class ListTopicPostsModel {
  protected $topicId;

  public function setTopicId( $topicId ) {
    $this->topicId = $topicId;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $query = $entityManager->createQuery( "SELECT p FROM entity\Post p JOIN entity\Topic t WHERE t.id = " . $this->topicId . " ORDER BY p.id DESC");
    $posts = $query->getResult();

    return $posts;
  }
}
