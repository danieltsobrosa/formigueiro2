<?php namespace model\post;

use \core;
use \entity;
use \model;

class ListHashtagsModel {
  protected $filter;

  public function setFilter($filter) {
    $this->filter = $filter;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $qb = $entityManager->createQueryBuilder();

    $qb->select("h.name", "count(h) as amount")
       ->from("entity\Hashtag", "h")
       ->groupBy("h.name");

    if (!is_null($this->filter)) {
      $qb->where("h.name like '%" . $this->filter . "%'");
    }

    $qb->orderBy("amount", "desc");

    $rs = $qb->getQuery()->getArrayResult();
    $hashtags = array();
    
    foreach ($rs as &$row) {
      $hashtag = new model\vo\HashtagStatsVO();
      $hashtag->setName($row["name"]);
      $hashtag->setAmount(intval($row["amount"]));

      array_push($hashtags, $hashtag);
    }

    return $hashtags;
  }
}
