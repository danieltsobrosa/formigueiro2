<?php namespace model\post;

use \core;
use \entity;
use \model\util;

class CreatePostModel {
  protected $message;
  protected $image;
  protected $geolocation;
  protected $owner;
  protected $topic;

  public function setMessage( $message ) {
    $this->message = $message;
  }

  public function setOwner( $owner ) {
    $this->owner = $owner;
  }

  public function setImage( $image ) {
    $this->image = $image;
  }

  public function setGeolocation( $geolocation ) {
    $this->geolocation = $geolocation;
  }

  public function setTopic( $topic ) {
    $this->topic = $topic;
  }

  public function execute() {
    // Cria o tópico
    $post = new entity\Post();
    $post->setMessage( $this->message );
    $post->setCreation( new \DateTime( "now" ) );
    $post->setOwner( $this->owner );
    $post->setImage( $this->image );
    $post->setGeolocation( $this->geolocation );
    $post->setTopic( $this->topic );

    $db = core\DatabaseManager::getInstance();
    $entityManager = $db->getEntityManager();

    // Insere o tópico
    $entityManager->persist( $post );
    $entityManager->flush();

    // Insere as mentions
    $mentionedUsers = util\PostUtil::findValidMentions($this->message);
    $entityManager = core\DatabaseManager::getInstance()->resetEntityManager();

    foreach ($mentionedUsers as &$user) {
      $mention = new entity\Mention();
      $mention->setIdMentionedUser($user->getId());
      $mention->setIdPost($post->getId());
      
      $entityManager->persist($mention);
    }
    
    $entityManager->flush();

    // Insere as hashtags
    $hashtags = util\PostUtil::findValidHashtags($this->message);
    $entityManager = core\DatabaseManager::getInstance()->resetEntityManager();

    foreach ($hashtags as &$name) {
      $hashtag = new entity\Hashtag();
      $hashtag->setIdPost($post->getId());
      $hashtag->setName($name);

      $entityManager->persist($hashtag);
    }

    $entityManager->flush();

    // Retorna o post
    return $post;
  }
}
