<?php namespace model\util;

use \model\session;

class SessionUtil {

  public static function isLogged( $authToken ) {
    $loggedUser = new session\LoggedUserModel();
    $loggedUser->setAuthToken( $authToken );
    $user = $loggedUser->execute();

    return $user;
  }
}
