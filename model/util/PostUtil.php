<?php namespace model\util;

use \model;

class PostUtil {
	private static $mentionRegex = '/@\w*/';
	private static $hashtagRegex = '/#\w*/';

	private static function isRepeatedUser(&$list, &$test) {
		foreach ($list as &$item) {
			if ($item->getId() == $test->getId()) {
				return true;
			}
		}

		return false;
	}

	private static function isRepeatedHashtag(&$list, &$test) {
		foreach ($list as &$item) {
			if ($item == $test) {
				return true;
			}
		}

		return false;
	}

	public static function findValidMentions($text) {
		$found = preg_match_all(static::$mentionRegex, $text, $matches);
		$users = array();

		if ($found) {
			$userModel = new model\user\GetUserByUsernameModel();

			foreach ($matches[0] as &$mention) {
				$username = substr($mention, 1);

				$userModel->setUsername($username);
				$user = null;

				try {
					$user = $userModel->execute();

					if (is_null($user))
						continue;

					if (static::isRepeatedUser($users, $user)) 
						continue;

				} catch (\Exception $e) {
					continue;
				}

				if (!is_null($user)) {
					array_push($users, $user);
				}
			}
		}

		return $users;
	}

	public static function findValidHashtags($text) {
		$found = preg_match_all(static::$hashtagRegex, $text, $matches);
		$hashtags = array();

		if ($found) {
			foreach ($matches[0] as &$item) {
				$hashtag = substr($item, 1);

				if (static::isRepeatedHashtag($hashtags, $hashtag))
					continue;

				array_push($hashtags, $hashtag);
			}
		}

		return $hashtags;
	}

	public static function validMentionsToLinks($text) {
		$found = preg_match_all(static::$mentionRegex, $text, $matches);

		if ($found) {
			foreach ($matches[0] as &$mention) {
				$username = substr($mention, 1);
				$link = "<a href='/usuario/" . $username . "/perfil' class='mention'>" . $mention . "</a>";

				$text =str_replace($mention, $link, $text);
			}
		}

		return $text;
	}

	public static function validHashtagsToLinks($text) {
		$found = preg_match_all(static::$hashtagRegex, $text, $matches);

		if ($found) {
			foreach ($matches[0] as &$item) {
				$hashtag = substr($item, 1);
				$link = "<a href='/hashtag/" . $hashtag . "/topicos"."' class='hashtag'>" . $item . "</a>";

				$text =str_replace($item, $link, $text);
			}
		}

		return $text;
	}

	public static function adjustNewLines($text) {
		return str_replace("\\n", "<br>", $text);
	}
}