<?php namespace model\topic;

use \core;
use \entity;

class GetTopicModel {
  private $id;

  public function setId( $id ) {
    $this->id = $id;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $topicRepository = $entityManager->getRepository( '\entity\Topic' );
    $topic = $topicRepository->findOneBy( array(
      "id" => $this->id
    ));

    return $topic;
  }
}
