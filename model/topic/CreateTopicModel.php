<?php namespace model\topic;

use \core;
use \entity;
use \model;

class CreateTopicModel {
  protected $title;
  protected $category;
  protected $owner;

  public function setTitle( $title ) {
    $this->title = $title;
  }

  public function setCategory($category) {
    $this->category = $category;
  }

  public function setOwner( $owner ) {
    $this->owner = $owner;
  }

  public function execute() {
    // Cria o tópico
    $topic = new entity\Topic();
    $topic->setTitle( $this->title );
    $topic->setCategory($this->category);
    $topic->setCreation( new \DateTime( "now" ) );
    $topic->setOwner( $this->owner );

    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();

    // Insere o tópico
    $entityManager->persist( $topic );
    $entityManager->flush();

    return $topic;
  }
}
