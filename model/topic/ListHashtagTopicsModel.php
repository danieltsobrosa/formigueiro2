<?php namespace model\topic;

use \core;
use \entity;
use \model;

class ListHashtagTopicsModel {
  protected $hashtag;
  protected $firstRow = 0;
  protected $countLimit = 50;

  public function setHashtag($hashtag) {
    $this->hashtag = $hashtag;
  }

  public function setFirstRow( $firstRow ) {
    $this->firstRow = $firstRow;
  }

  public function setCountLimit( $countLimit ) {
    $this->countLimit = $countLimit;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();

    // Busca as ocorrências desta hashtag
    $hashtags = $entityManager->getRepository( '\entity\Hashtag' )->findBy(array(
      "name" => $this->hashtag
    ));

    if (count($hashtags) == 0) {
      return array();
    }

    // Busca os posts com esta hashtag
    $postIds = array();

    foreach ($hashtags as &$hashtag) {
      $postId = $hashtag->getIdPost();

      if (!in_array($postId, $postIds)) {
        array_push($postIds, $postId);
      }
    }
    $qb = $entityManager->createQueryBuilder();

    $qb->select("p")
       ->from("entity\Post", "p")
       ->where("p.id in(" . implode("," , $postIds) . ")");

    $posts = $qb->getQuery()->getResult();

    if (count($posts) == 0) {
      return array();
    }
    
    // Pega os tópicos destes posts
    $topics = array();

    foreach ($posts as &$post) {
      $topic = $post->getTopic();

      if (!in_array($topic, $topics) && !$topic->getClosed()) {
        array_push($topics, $topic);
      }
    }
    
    return $topics;
  }
}
