<?php namespace model\topic;

use \core;
use \entity;
use \model;

class FinishTopicModel {
  protected $id;

  public function setId($id) {
    $this->id = $id;
  }

  public function execute() {
    // Busca o tópico
    $getTopic = new GetTopicModel();
    $getTopic->setId($this->id);
    $topic = $getTopic->execute();

    $topic->setClosed(true);

    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();

    $entityManager->merge($topic);
    $entityManager->flush();

    return $topic;
  }
}
