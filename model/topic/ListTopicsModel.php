<?php namespace model\topic;

use \core;
use \entity;
use \model;

class ListTopicsModel {
  protected $firstRow = 0;
  protected $countLimit = 50;

  public function setFirstRow( $firstRow ) {
    $this->firstRow = $firstRow;
  }

  public function setCountLimit( $countLimit ) {
    $this->countLimit = $countLimit;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    $query = $entityManager->createQuery( "SELECT t FROM entity\Topic t" );
    $query->setFirstResult( $this->firstRow );
    $query->setMaxResults( $this->countLimit );
    $topics = $query->getResult();

    return $topics;
  }
}
