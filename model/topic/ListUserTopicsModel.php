<?php namespace model\topic;

use \core;
use \entity;
use \model;

class ListUserTopicsModel {
  protected $username;
  protected $firstRow = 0;
  protected $countLimit = 50;

  public function setUsername( $username ) {
    $this->username = $username;
  }

  public function setFirstRow( $firstRow ) {
    $this->firstRow = $firstRow;
  }

  public function setCountLimit( $countLimit ) {
    $this->countLimit = $countLimit;
  }

  public function execute() {
    $entityManager = core\DatabaseManager::getInstance()->getEntityManager();
    // $sql = "SELECT t FROM entity\Topic t JOIN entity\User u WHERE u.username = '" . $this->username . "' and t.closed = false order by t.id desc";

    $user = $entityManager->getRepository( '\entity\User' )->findOneBy(array(
      "username" => $this->username
    ));

    $repo = $entityManager->getRepository( '\entity\Topic' );
    $topics = $repo->findBy( array(
      "closed" => false,
      "owner" => $user->getId()
    ));

    // $query = $entityManager->createQuery($sql);
    // $query->setFirstResult( $this->firstRow );
    // $query->setMaxResults( $this->countLimit );
    // $topics = $query->getResult();

    return $topics;
  }
}
