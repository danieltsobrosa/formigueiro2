<?php namespace model\vo;

use \core;

class HashtagStatsVO implements \JsonSerializable {
	use core\serializableTrait;

	protected $name;
	protected $amount;

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getAmount() {
		return $this->amount;
	}

	public function setAmount($amount) {
		$this->amount = $amount;
	}
}