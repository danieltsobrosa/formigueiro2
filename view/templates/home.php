<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Home</title>
    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>
    
    <link href="/view/css/home.css" rel="stylesheet">
  </head>

  <body>

    <?php include( "/view/layouts/header.php" ); ?>

    <section class="container conteudo">
        <div class="col-md-9 hidden-xs hidden-sm tam-conteudo">
          <!-- espaço vazio para banner -->
        <br>
         <img src="/view/images/home/banner.png" class="img-rounded">
         <h1 class= "textoIntro">IDEIAS UNIDAS PARA <br>SOLUÇÃO DE PROBLEMAS</h1>

        <?php include( "/view/layouts/busca-field.php" ); ?>  
              
        </div>

        <div class="col-sm-6 col-sm-offset-3 col-md-3 col-md-offset-0 painel-login tam-conteudo">
          <form method="POST" action="/home" name="form_login" id="form_login">
            <br><br><br>
            <div class= "row hcenter">
              <div class="form-group">
                <label>Usuário</label>
                <input type="text" class="form-control input-center" id="user" name="username" required>
              </div>
            </div>
            <br>

            <div class= "row hcenter">
              <div class="form-group">
                <label>Senha</label>
                <input type="password" class="form-control input-center" id="password" name="password" onblur= "validaPassword()" required>
              </div>
            </div>
            <br><br>

            <button type="submit" class="btn btn-success btnconfirm glyphicon glyphicon-menu-right" id="btnconfirm">
              Entrar
            </button>
            <br><br>
            <div class="registre-se">
              Não tem acesso? <a href="/registro-usuario">
                                  Registre-se
                              </a>
            </div><br>
            <script src="/view/pagecontrol/padrao.js"></script>
            <?php if (isset($errorMessage) && $errorMessage == "invalid-user-or-password") {?>
              <div class="alert alert-danger" role="alert">Usuário ou senha invalidos!</div>
            <?php } ?>
            <br>
          </form>
        </div>
    </section>

    <div class="spacer"></div>
    <?php include( "/view/layouts/footer.php" ); ?>
    <?php include( "/view/includes/scripts.php" ); ?>
    <script src="/view/pagecontrol/home.js"></script>
  </body>
</html>
