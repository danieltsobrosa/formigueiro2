<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Atualizações</title>

    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>
    <link href="/view/css/principal.css" rel="stylesheet">
  </head>
  <body>
    <?php include( "/view/layouts/header.php" ); ?>

    <section class="container">
      <h2 id="content-title" style="text-align: center">Atualizações</h2>
      <?php include( "/view/layouts/side-menu.php" ); ?>

      <section class="col-xs-12 col-sm-8 col-md-9">        

				<?php include( "/view/layouts/topic-list.php" ); ?>
      </section>
    </section>

    <div class="spacer"></div>
    <?php include( "/view/layouts/footer.php" ); ?>
    <?php include( "/view/includes/scripts.php" ); ?>
    <script src="view/pagecontrol/principal.js"></script>
  </body>
</html>
