<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Tópicos com #<?= $_PARAMS['hashtag']; ?></title>

    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>
    <link href="/view/css/principal.css" rel="stylesheet">
  </head>
  <body>
    <?php include( "/view/layouts/header.php" ); ?>

    <section class="container">
      <section class="col-xs-12 col-sm-10 col-md-8 col-xs-offset-0 col-sm-offset-1 col-md-offset-2">

        <h2 id="content-title">Tópicos com a hashtag #<?= $_PARAMS['hashtag']; ?></h2>
        
				<?php include( "/view/layouts/topic-list.php" ); ?>
      </section>
    </section>

    <div class="spacer"></div>
    <?php include( "/view/layouts/footer.php" ); ?>
    <?php include( "/view/includes/scripts.php" ); ?>
    <script src="view/pagecontrol/principal.js"></script>
  </body>
</html>
