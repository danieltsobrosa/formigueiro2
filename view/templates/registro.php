<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Registro</title>

    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>

  </head>

  <body>

    <?php include( "/view/layouts/header.php" ); ?>
    <form method="POST" action="/registro-usuario">
      <section class="container" style="margin-top: -18px">
        <div class = "row" style="position:relative">
        <?php if(!is_null($error)){?>
            <div class="alert alert-danger" role="alert" style="text-align: center"> <?= $error?> </div>
        <?php } ?>

          <div class = "row">
              <h2 class="text-center">Informe seus dados</h2>
              <div class = "col-xs-6 col-xs-offset-3 col-md-6 col-md-offset-3">

                <div class= "row">
                  <div class="form-group">
                    <label>Nome completo</label>
                    <input type="text" class="form-control" id="name" name="fullname" required>
                  </div>
                </div>

                <div class= "row">
                  <div class="form-group">
                    <label>Email --opcional--</label>
                    <input type="text" class="form-control" id="email" name="email">
                  </div>
                </div>

                <div class= "row">
                  <label>Sexo</label><br>
                  <label class="radio-inline"><input type="radio" name="sex" value="true" required>Masculino</label>
                  <label class="radio-inline"><input type="radio" name="sex" value="false" required>Feminino</label>
                  <br><br>
                </div>

                <!--Aqui as cidades e estados-->
                <div class= "row">
                  <div class="col-md-4" style="padding-left: 0">
                    <div class="form-group">
                      <label for="selectcity">Estado</label>
                      <select class="form-control scrollable-menu-small" id="selectcity" name="state">
                        <option>AC</option>
                        <option>AL</option>
                        <option>AP</option>
                        <option>AM</option>
                        <option>BA</option>
                        <option>CE</option>
                        <option>DF</option>
                        <option>ES</option>
                        <option>GO</option>
                        <option>MA</option>
                        <option>MT</option>
                        <option>MS</option>
                        <option>MG</option>
                        <option>PA</option>
                        <option>PB</option>
                        <option>PR</option>
                        <option>PE</option>
                        <option>PI</option>
                        <option>RJ</option>
                        <option>RN</option>
                        <option>RS</option>
                        <option>RO</option>
                        <option>RR</option>
                        <option>SC</option>
                        <option>SP</option>
                        <option>SE</option>
                        <option>TO</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-8" style="padding-right: 0; padding-left: 0">
                    <label>Município</label>
                    <input type="text" class="form-control" id="city" name="city" required>
                  </div>
                </div>
                <br>

                <div class= "row">
                  <div class="form-group">
                    <label>Usuário</label>
                    <input type="text" class="form-control" id="username" name="username" required>
                  </div>
                </div>

                <div class= "row">
                  <div class="form-group">
                    <label>Senha</label>
                    <input type="password" class="form-control" id="password" onblur= "verifyPassword()" name="password" required>
                  </div>
                </div>

                <div class= "row">
                  <div class="form-group">
                    <label>Confirme sua senha</label>
                    <input type="password" class="form-control" id="passwordconfirm" onblur= "verifyPasswordConfirm()" required>
                  </div>
                </div>

                <div class= "row">
                  <div class="form-group">
                    <label>Fale mais sobre você... <small>(max 250 caracteres)</small></label>
                    <textarea class="form-control" style="resize: none; height: 100px" contenteditable="false" id= "userBio" name="about" oninput= "verifyUserBioLenght()" ></textarea>
                  </div>
                </div>
              </div>
        </div>
        <!--Botões de finalização-->
        <div class = "row col-md-12" style="text-align: center">
          <button type="submit" class="btn btn-success" id="btnconfirm">
            <span class="glyphicon glyphicon-ok"></span> Cadastrar
          </button>
          <a href="#" class="btn btn-danger">
            <span class="glyphicon glyphicon-remove"></span> Cancelar
          </a>
        </div>
      </section><br>
    </form>

    <?php include( "/view/layouts/footer.php" ); ?>
    <script src="/view/pagecontrol/registro.js"></script>
    <?php include( "/view/includes/scripts.php" ); ?>
  </body>

</html>
