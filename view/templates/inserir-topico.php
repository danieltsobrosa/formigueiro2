<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Postagem</title>

    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>
  </head>

  <body>

    <?php include( "/view/layouts/header.php" ); ?>

    <section class="container">
      <br>
      <h3 class="text-center">Novo Tópico:</h3>
      <br>

      <form method="POST" action="/criar-topico" enctype="multipart/form-data"
          class="horizontal-form col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Título</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="inpuTitulo" name="title" required>
          </div>
        </div>
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Categoria</label>
          <div class="col-sm-10">
            <select class="form-control" name="category">
                <option>Correção</option>
                <option>Sugestão</option>
                <option>Reclamação</option>
              </select>
          </div>
        </div>
            
        <?php include( "/view/layouts/novo-post.php" ); ?>

          </div>    
          <!--Botões de finalização-->
          <div class = "row text-right">
            <button type="submit" class="btn btn-success" id="btnconfirm">
              <span class="glyphicon glyphicon-ok"></span> Postar
            </button>
          </div>
      </form>
    </section>

    <?php include( "/view/includes/scripts.php" ); ?>
    <script src="/view/pagecontrol/registro.js"></script>
    <script src="/view/pagecontrol/inseretopico.js"></script>
    <script src="/view/pageControl/novo-post.js"></script>
  </body>

</html>
