<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Registro</title>

    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>

    <link href="/view/css/EditProfile.css" rel="stylesheet">

  </head>

  <body>

    <?php include( "/view/layouts/header.php" ); ?>
    <br>
    <form method="POST" action="/usuario/editar-perfil" enctype="multipart/form-data">
      <section class="container">
        </div class = "row" style="position:relative">

          <div class = "row">
              <h1>Altere seus dados</h1>
              <br><br>
              <div class = "col-md-6">

                <div class= "row">
                  <div class="form-group">
                    <label>Nome completo</label>
                    <input type="text" class="form-control" id="name" name="fullname" value="<?= $loggedUser->getFullname()?>" required>
                  </div>
                </div>

                <div class= "row">
                  <div class="form-group">
                    <label>Email --opcional--</label>
                    <input type="text" class="form-control" id="email" name="email" value="<?= $loggedUser->getEmail()?>">
                  </div>
                </div>

                <div class= "row">
                  <label>Sexo</label><br>
                  <label class="radio-inline"><input type="radio" name="sex" value="true" <?= $masculinoChecked ?> required>Masculino</label>
                  <label class="radio-inline"><input type="radio" name="sex" value="false" <?= $femininoChecked ?> required>Feminino</label>
                  <br><br>
                </div>

                <!--Aqui as cidades e estados-->
                <div class= "row">
                  <div class="col-md-4" style="padding-left: 0">
                    <div class="form-group">
                      <label for="selectcity">Estado</label>
                      <select class="form-control scrollable-menu-small" id="selectcity" name="state">
                        <option <?= $isState("AC") ?>>AC</option>
                        <option <?= $isState("AL") ?>>AL</option>
                        <option <?= $isState("AP") ?>>AP</option>
                        <option <?= $isState("AM") ?>>AM</option>
                        <option <?= $isState("BA") ?>>BA</option>
                        <option <?= $isState("CE") ?>>CE</option>
                        <option <?= $isState("DF") ?>>DF</option>
                        <option <?= $isState("ES") ?>>ES</option>
                        <option <?= $isState("GO") ?>>GO</option>
                        <option <?= $isState("MA") ?>>MA</option>
                        <option <?= $isState("MT") ?>>MT</option>
                        <option <?= $isState("MS") ?>>MS</option>
                        <option <?= $isState("MG") ?>>MG</option>
                        <option <?= $isState("PA") ?>>PA</option>
                        <option <?= $isState("PB") ?>>PB</option>
                        <option <?= $isState("PR") ?>>PR</option>
                        <option <?= $isState("PE") ?>>PE</option>
                        <option <?= $isState("PI") ?>>PI</option>
                        <option <?= $isState("RJ") ?>>RJ</option>
                        <option <?= $isState("RN") ?>>RN</option>
                        <option <?= $isState("RS") ?>>RS</option>
                        <option <?= $isState("RO") ?>>RO</option>
                        <option <?= $isState("RR") ?>>RR</option>
                        <option <?= $isState("SC") ?>>SC</option>
                        <option <?= $isState("SP") ?>>SP</option>
                        <option <?= $isState("SE") ?>>SE</option>
                        <option <?= $isState("TO") ?>>TO</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-md-8" style="padding-right: 0; padding-left: 0">
                    <label>Município</label>
                    <input type="text" class="form-control" id="city" value="<?= $loggedUser->getCity()?>" name="city" required>
                  </div>
                </div>
                <br>

                <div class= "row">
                  <div class="form-group">
                    <label>Sobre você... <small>(max 250 caracteres)</small></label>
                    <textarea class="form-control" style="resize: none; height: 100px" id= "userBio" name="about" oninput= "verifyUserBioLenght()" ><?= $loggedUser->getAbout()?></textarea>
                  </div>
                </div>
              </div>

            <div class = "col-md-4 col-md-offset-1 text-vcenter">
              <h3 id="content-title">Upload de Avatar</h3>
              <img src="/view/images/profile/<?= $loggedUser->getAvatar(); ?>" class="img-rounded" id="imgAvatar">
              <br>
              <h5>
                <small>
                  Faça upload de alguma imagem de sua preferência.<br>
                  Essa imagem será exibida em seu perfil.
                </small>
              </h5>
              <span class="btn btn-default btn-file" width="75%">
                Selecionar arquivo...
                <input type="file" accept="image/*" name="avatar" id="fileAvatar">
              </span>
            </div>
          </div>

          <!--Botões de finalização-->
          <div class = "row">
            <br>
            <button type="submit" class="btn btn-success" id="btnconfirm">
              <span class="glyphicon glyphicon-ok"></span> Salvar
            </button>
            <a href="/usuario/<?= $loggedUser->getUserName()?>/perfil" class="btn btn-danger">
              <span class="glyphicon glyphicon-remove"></span> Cancelar
            </a>
          </div>
        </div>
      </section>
    </form>

    <br><br>
    <div class="spacer"></div>
    <?php include( "/view/includes/scripts.php" ); ?>
    <?php include( "/view/layouts/footer.php" ); ?>
    <?php include( "/view/includes/scripts.php" ); ?>
    <script src="/view/pagecontrol/registro.js"></script>
    <script src="/view/pagecontrol/editProfile.js"></script>
    <?php include( "/view/includes/styles.php" ); ?>
  </body>

</html>
