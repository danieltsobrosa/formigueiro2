<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Resultados da Busca</title>

    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>
    <link href="/view/css/home.css" rel="stylesheet">
  </head>
  <body>
    <?php include( "/view/layouts/header.php" ); ?>

    <section class="container">
      <section class="col-xs-12 col-sm-8 col-md-8 col-md-offset-2">
        <?php include( "/view/layouts/busca-field.php" ); ?>

        </br>

        <?php include( "/view/layouts/user-list.php" ); ?>
      </section>

    </section><!--container -->
    <br>
    <?php include( "/view/includes/scripts.php" ); ?>
  </body>
</html>
