<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Perfil do Usuário</title>

    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>
  </head>
  <body>
    <?php 
    
      include( "/view/layouts/header.php" ); 
    ?>

    <section class="container">
      <h2 id="content-title" style="text-align: center">Perfil do Usuário</h2>

      <?php include( "/view/layouts/profile-side-menu.php" ); ?>

      <section class="col-xs-12 col-sm-12 col-md-9">    

				<?php include( "/view/layouts/topic-list.php" ); ?>
      </section>
    </section>

    <?php include( "/view/includes/scripts.php" ); ?>
  </body>
</html>
