<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Tópico</title>

    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>
    <link href="/view/css/topico.css" rel="stylesheet">
  </head>
  <body>
    <?php include( "/view/layouts/header.php" ); ?>

    <section class="container">
      <!-- <?php include( "/view/layouts/side-menu.php" ); ?> -->

      <section class="col-xs-12 col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">


        <h2 id="content-title" class="topic-top">
          <img src="/view/images/profile/<?= $topic->getOwner()->getAvatar(); ?>" class="img-circle avatar-small user-avatar-clicable"
           title="<?= $topic->getOwner()->getFullname(); ?>"
           data-username="<?= $topic->getOwner()->getUsername(); ?>">

          <small class="pull-right"><?= $topic->getCategory() ?></small>
          <?= $topic->getTitle() ?>
          <br>
          <!-- PRIMEIRO POST, QUE EH CONSIDERADO A DESCRICAO -->
          <span class="fonte-simples">
            <?php echo $topic->getPosts()[0]->getMessage(); ?>
          </span> 
          <?php if (!is_null($topic->getPosts()[0]->getImage())) { ?>
            <div class="row">
              <center><img src="/view/images/posts/<?= $topic->getPosts()[0]->getImage(); ?>"></center>
            </div>
          <?php } ?>
        </h2>
        <br>

        <!--PROXIMOS POSTS-->
				<?php include( "/view/layouts/post-list.php" ); ?>

        <br><center>Faça um comentário</center><br>
        <!-- Esse é o campo de comentar -->
        <form method="POST" action="/topico/<?= $topic->getId(); ?>" enctype="multipart/form-data" class="form-horizontal">
          <?php include( "/view/layouts/novo-post.php" ); ?>
          <div class="text-right">
            <button class="btn btn-primary" type="submit">Comentar</button>
          </div>
        </form>

        <br>

        <?php if (!$topic->getClosed()) { ?>
          <form class="row text-center" method="POST" action="/topico/<?= $topic->getId(); ?>/concluir">
            <hr>
            <button type="submit" id="concluir" class="btn btn-danger">
              Concluir
            </button>
          </form>
        <?php } ?>
      </section>
    </section>

    <?php include( "/view/includes/scripts.php" ); ?>
    <script src="/view/pageControl/novo-post.js"></script>
  </body>
</html>
