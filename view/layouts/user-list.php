
<div class="list-group col-sm-3">
  <a class="list-group-item active">
    Hashtags
  </a>

  <!-- Listagem de hashtags -->
  <?php foreach ( $hashtags as &$hashtag ) { ?>

    <a href="/hashtag/<?= $hashtag->getName(); ?>/topicos" class="list-group-item">
      <small class="pull-right">
        <?= $hashtag->getAmount(); ?>
      </small>
      <p class="list-group-item-text">
           <?= $hashtag->getName(); ?>&nbsp;
      </p>
    </a>

  <?php } ?>

  <i>
    <b>
      <?php 
      $hashtagCount = count($hashtags);

      if ($hashtagCount == 0) { ?>
        Nenhuma hashtag encontrada
      <?php } else if ($hashtagCount == 1) { ?>
        1 hashtag encontrada
      <?php } else { ?>
        <?= $hashtagCount; ?> hashtags encontradas
      <?php } ?>
    </b>
  </i>
</div>

<div class="list-group col-md-9">
  <a class="list-group-item active">
    Usuários
  </a>

  <!-- Listagem de usuários -->
  <?php foreach ( $users as &$user ) { ?>

    <a href="/usuario/<?= $user->getUsername(); ?>/perfil" class="list-group-item">
      <div class="row">
        <div class="col col-xs-5 col-sm-4 col-md-3">
          <img src="/view/images/profile/<?= $user->getAvatar() ?>" class="img-circle avatar-topico">
        </div>
        <div class="col col-xs-7 col-sm-8 col-sm-9">
          <?= $user->getFullname() ?><br>
          <small>Mora em <?= $user->getCity() ?>, <?= $user->getState() ?></small><br>
          <small><?= $user->getAbout() ?></small><br>
        </div>
      </div>
    </a>
  <?php } ?>

  <i>
    <b>
      <?php 
      $usersCount = count($users);

      if ($usersCount == 0) { ?>
        Nenhum usuário encontrado
      <?php } else if ($usersCount == 1) { ?>
        1 usuário encontrado
      <?php } else { ?>
        <?= $usersCount; ?> usuário encontrados
      <?php } ?>
    </b>
  </i>
</div>
