<section id="sidemenu" class="col-xs-12 col-sm-4 col-md-3 img-thumbnail">
  Categorias	
  <br><br>
  <ul class="nav nav-pills nav-stacked">
  	<li id="Todos" role="presentation" class="categoria">
      <a href="/principal?categoria=Todos">Todos</a>
    </li>
    <li id="Correção" role="presentation" class="categoria">
      <a href="/principal?categoria=Correção">Correção</a>
    </li>
    <li id="Sugestão" role="presentation" class="categoria">
      <a href="/principal?categoria=Sugestão">Sugestão</a>
    </li>
    <li id="Reclamação" role="presentation" class="categoria">
      <a href="/principal?categoria=Reclamação">Reclamação</a>
    </li>
  </ul>
  <br><br><br>
  Utilidades
  <br><br>
  <?php include( "/view/layouts/busca-field.php" ); ?>
  <br>
  Top 5 Hashtags
  <br><br>

  <?php if (isset($hashtags)) {
    $pos = 1;

    foreach ($hashtags as &$hashtag) { ?>
      <?= $pos; ?>º
      <a href="/hashtag/<?= $hashtag->getName(); ?>/topicos">
        #<?= $hashtag->getName(); ?>
      </a>
      <br>
    <?php
      $pos++;
    }
  } ?>

  <br>

</section>