
<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">Comentário</label>
  <div class="col-sm-10">
    <textarea class="form-control" rows="3" name="message" required></textarea>
  </div>
</div>
<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">Imagem</label>
  <div class="col-sm-5">
    <input type="file" name="image" accept="image/*">
  </div>
  <div class="col-sm-offset-1 col-sm-4 text-right">
    <label style="font-weight: normal">
      <input type="checkbox" disabled="disabled" name="geolocation"> Informar minha localização
    </label>
    </div>
  </div>
</div>