<section id="sidemenu" class="col-xs-9 col-sm-6 col-sm-offset-4 col-xs-offset-2 col-md-offset-0 col-md-3">

  <div class="img-thumbnail ">
	  <br>
	  <img src="/view/images/profile/<?= $user->getAvatar() ?>" style="max-width:245px ; width: 100%; heigth: 230px;" >
	  <br>
	  <h3><?= $user->getFullname() ?></h3>
	  <br>
	  <small><?= $user->getEmail() ?></small>
	  <br>
	  <small><?= $user->getCity() ?>, <?= $user->getState() ?></small> 
	  <br>
	  <small><?= $user->getGenderDescription() ?></small>
	  <br>
	  <small><?= $user->getAbout() ?></small>
	  <br><br><br><br>
	  <div align="center">
		  	<?php //testa se o usuário logado é igual ao perfil visualizado
		  	if (!is_null($loggedUser)) {
			  	if ($user->getId() != $loggedUser->getId() ) { 
			  		//aqui ele testa se a variavel like esta vazia, se sim é porque o usuario atual não seguiu o usuario do perfil visitado
			  		if(is_null( $like )){ ?>

					  	<form method="POST" action="/usuario/<?= $user->getUsername() ?>/perfil" style="text-align: center">
					      <button type="submit" class="btn btn-primary" style="width: 80%">Seguir</button>
					    </form>

			  		<?php } else {?>

					  	<form method="POST" action="/usuario/<?= $user->getUsername() ?>/perfil" style="text-align: center">
					      <button type="submit" class="btn btn-danger" style="width: 150px">Deixar de Seguir</button>
					    </form>

				    <?php }
				} else{ ?>
					<a href="/usuario/editar-perfil">
						<button type="submit" class="btn btn-success" style="width: 80%; ">Editar</button>
					</a>
			    <?php }?>
			<?php } ?>
	  </div>
	  <br><br>
  </div>

</section>
