<nav id="header" class="navbar navbar-default">
  <div class="container-fluid container">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header col-xs-5">		
    	<a class="navbar-brand" href="/home">
    		<nobr>
				<img src="/view/images/header/icon.png" id="logo-header"> 
	    		Formigueiro
	    	</nobr>
    	</a>
    </div>

    <?php if (!is_null(@$loggedUser)) { ?>

    	<div class="logged-user-panel pull-right">
	      <nobr>
	        <a href="/usuario/<?= $loggedUser->getUsername() ?>/perfil">
	          <img src="/view/images/profile/<?= $loggedUser->getAvatar() ?>" class="img-circle avatar-header">
              <?= $loggedUser->getFullname() ?>
	        </a>
	        <form method="POST" action="/home">
	          <input type="hidden" name="logoff" value="true">
	          <button type="submit" class="btn btn-danger btn-xs">
	            Sair
	          </button>
	        </form>
	      </nobr>
	    </div>

	    <a href="/criar-topico" id="btn-criar-topico" class="btn btn-success pull-right">
	  		Criar tópico
	    </a>


	<?php } ?>    

  </div><!-- /.container-fluid -->
</nav>
