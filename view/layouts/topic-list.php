<div class="list-group">

  <?php 
    if (empty($topics)) { ?>
      <div class="list-group-item">
          Não existe nenhum tópico aberto no momento, crie algo novo!
      </div>
    <?php }else{
      foreach ( $topics as &$topic ) {
      ?>
      <a href="/topico/<?= $topic->getId(); ?>" class="list-group-item">
        <small class="pull-right">
          <?= $topic->getOwner()->getFullname(); ?> em 
          <?= $topic->getCreation()->format( "d/m/Y H:i" ); ?>
        </small>
        <h4 class="list-group-item-heading">
          <img src="/view/images/profile/<?= $topic->getOwner()->getAvatar() ?>" class="img-circle avatar-small user-avatar-clicable"
             title="<?= $topic->getOwner()->getFullname(); ?>"
             data-username="<?= $topic->getOwner()->getUsername(); ?>">
          <?= $topic->getTitle(); ?>
        </h4>
        <p class="list-group-item-text row">
          <small class="col-md-6 text-left"><?= ($topic->getPosts()->count()) -1; ?> comentários</small>
          <small class="col-md-6 text-right">
            <span class="tag-correcao">
              <?= $topic->getCategory(); ?>
            </span>
          </small>
        </p>
      </a><br>
  <?php };}?>

</div>
