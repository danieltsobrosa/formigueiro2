<nav id="header" class="navbar navbar-default">
  <div class="container-fluid container">

    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
    	<a class="navbar-brand" href="/principal">
			<img src="/view/images/header/icon.png" id="logo-header">
    		Formigueiro
    	</a>
    </div>     

    <div class="logged-user-panel pull-right">
      <nobr>
        <a href="/usuario/<?= $loggedUser->getUsername() ?>/perfil">
          <img src="/api/user/<?= $loggedUser->getId() ?>/avatar">
          <?= $loggedUser->getFullname() ?>
        </a>
        <form method="POST" action="/home">
          <input type="hidden" name="logoff" value="true">
          <button type="submit" class="btn btn-danger btn-xs">
            Sair
          </button>
        </form>
      </nobr>
    </div>

  </div><!-- /.container-fluid -->
</nav>
