<ul class="list-group">

  <?php foreach ( $topic->getPosts() as &$post ){ 
      if($topic->getPosts()[0]->getId() == $post->getId()){
          //quando é o primeiro post ele não faz nada!
    }else{?>

    <li class="list-group-item">
      <small class="pull-right">
        <?= $post->getOwner()->getFullname(); ?> em 
        <?= $post->getCreation()->format( "d/m/Y H:i" ); ?>
      </small>

      <?php if (!is_null($post->getGeolocation())) { ?>
        <a class="pull-right" href="https://www.google.com.br/maps/@<?= $post->getGeolocation(); ?>,13z"
            style="margin-right: 5px; margin-top: -1px" target="_blank"
            title="Veja no mapa a localização">
          <span class="glyphicon glyphicon-globe">
        </a>
      <?php } ?>

      <img src="/view/images/profile/<?= $post->getOwner()->getAvatar(); ?>" class="img-circle avatar-small user-avatar-clicable"
           title="<?= $post->getOwner()->getFullname(); ?>"
           data-username="<?= $post->getOwner()->getUsername(); ?>">

      <?= $prepareMessage($post->getMessage()); ?>

      <?php if (!is_null($post->getImage())) { ?>
      	<br><br>
      	<div class="row">
      		<center><img src="/view/images/posts/<?= $post->getImage(); ?>" width="90%"><center>
      	</div>
      <?php } ?>
    </li><br>

  <?php }} ?>

</ul>
