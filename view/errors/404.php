<!DOCTYPE html>
<html lang="pt">
  <head>
    <title>Página não encontrada</title>

    <?php include( "/view/includes/meta.php" ); ?>
    <?php include( "/view/includes/styles.php" ); ?>
  </head>
  <body>
    <?php include( "/view/layouts/header.php" ); ?>

    <section class="container">
      <section class="col-xs-12 col-sm-offset-2 col-sm-8">

        <h2 id="content-title" class="text-center">Página não encontrada</h2>

      </section>
    </section>

    <?php include( "/view/includes/scripts.php" ); ?>
  </body>
</html>
