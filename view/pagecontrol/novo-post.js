
(function() {
	if ("geolocation" in navigator) {
		navigator.geolocation.getCurrentPosition(function(position) {
		  var geo = position.coords.latitude + "," + position.coords.longitude;
		  $("input[name=geolocation]").attr("disabled", false).val(geo);
		});
	}
}());