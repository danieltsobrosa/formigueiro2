//$(function()); ele executa sempre, sem precisar ser chamado no html por funções
$(function() {
	setTimeout(function() {
		$(".alert").fadeOut();
	}, 5000);
});

// Permite clicar nas fotos dos usuários
$(function() {
	$(".user-avatar-clicable").click(function(event) {
		window.location = "/usuario/" + $(this).data().username + "/perfil";
		return false;
	});
});