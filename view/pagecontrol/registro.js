function verifyPassword(){
	var pass1;

	pass1 = $("#password").val();

	//função que testa força minima da senha
	if(pass1.length != 0){
		if(pass1.length < 6){
			alert("As senhas devem ter no mínimo 6 caracteres!");
			$("#password").focus();
			return false;
		}
		else{
			return true;
		}
	}
}

function verifyPasswordConfirm(){
	var pass1, pass2;

	//função que testa se senhas são iguais
	pass1 = document.getElementById("password").value;
	pass2 = document.getElementById("passwordconfirm").value;

	if(pass1 != pass2){
		alert("As senhas não são iguais!");
		$("#passwordconfirm").focus();
		return false;
	}
	else{
		return true;
	}
}

function verifyUserBioLenght(){
	var bio;
	var aux;

	//função para testar limites de campos
	bio = $("#userBio").val();
	if(bio.length > 250){
		aux = bio.slice(0,-1);
		$("#userBio").val(aux);
	}
}