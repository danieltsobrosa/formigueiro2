
// Ajusta o filtro de categoria
$(function() {
	var categoria = $.url().param('categoria') || 'Todos';

	$('.categoria').removeClass('active');
	$('#' + categoria).addClass('active');
});